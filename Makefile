# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: victortudes <victortudes@student.42.fr>    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/09/30 21:43:16 by victortudes       #+#    #+#              #
#    Updated: 2021/09/30 21:53:23 by victortudes      ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ping
GCC = gcc -c -Wall -Werror -Wextra
PATH_INC = include/
INC = $(addprefix $(PATH_INC), ft_ping.h)
PATH_INC_LIB = libft/
PATH_SRC = sources/
PATH_OBJ = obj/
PATH_LIB = libft/
LIB = -L $(PATH_LIB) -lft

SRC_FILE = 	main.c \
			address.c \
			display.c \
			flags.c \
			loop.c \
			signal.c \
			socket.c \
			time.c \

ALL_OBJ = $(addprefix $(PATH_OBJ), $(SRC_FILE:.c=.o))


define compile
mkdir -p $(PATH_OBJ)
@$(GCC) $1 -o $2 -I $(PATH_INC) -I $(PATH_INC_LIB)
endef

all: $(NAME)

$(NAME): $(ALL_OBJ)
	@make --no-print-directory -C $(PATH_LIB)
	@gcc $(ALL_OBJ) -I $(PATH_INC) -I $(PATH_INC_LIB) $(LIB) -o $(NAME) -lm

$(PATH_OBJ)%.o: $(PATH_SRC)%.c $(INC)
	@$(call compile, $< ,$@)

clean:
	@make --no-print-directory  clean -C $(PATH_LIB)
	@rm -rf $(PATH_OBJ)

fclean: clean
	@make  --no-print-directory fclean -C $(PATH_LIB)
	@rm -rf $(NAME)

re: fclean all

.PHONY: all re clean fclean#